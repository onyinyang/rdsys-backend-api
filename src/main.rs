use sha1::{Sha1, Digest};

fn get_uid(fingerprint: String, pt_type: String) -> Result<u64, hex::FromHexError> {
    let hex_fingerprint = match hex::decode(fingerprint) {
        Ok(hex_fingerprint) => hex_fingerprint,
        Err(e) => return Err(e),
    };

    let mut hasher = Sha1::new();
    hasher.update(hex_fingerprint);
    let result_fingerprint = hasher.finalize();
    let uid_string = pt_type+&hex::encode_upper(result_fingerprint);
    Ok(crc64::crc64(0, uid_string.as_bytes()))
    }


fn main() {
let hex_stuff = get_uid("FD8DC7EF92F1F14D00CF9D6F6297A3468B59E707".to_string(), "obfs4".to_string());
println!("The result is: {:?}", hex_stuff);
let hex_stuff2 = get_uid("FD8DC7EF92F1F14D00CF9D6F6297A3468B59E707".to_string(), "scramblesuit".to_string());
println!("The result is: {:?}", hex_stuff2);
}